import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { DataDetailsComponent } from './data-details/data-details.component';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import {
  IgxCategoryChartModule,
  IgxLegendModule
} from "igniteui-angular-charts";
import { NgxEchartsModule } from 'ngx-echarts';
import { EchartService } from './echart.service';
import { HttpClientModule } from '@angular/common/http';
import { ChartsModule } from 'ng2-charts';

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    DataDetailsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    NgbModule,
    FormsModule,
    BrowserAnimationsModule,
    IgxCategoryChartModule,
    IgxLegendModule,
    NgxEchartsModule.forRoot({
      echarts: () => import('echarts')
    }),
    HttpClientModule,
    ChartsModule
  ],
  providers: [EchartService],
  bootstrap: [AppComponent]
})
export class AppModule { }
