import { Component, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { EchartService } from '../echart.service';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { Color, Label } from 'ng2-charts';
@Component({
  selector: 'app-data-details',
  templateUrl: './data-details.component.html',
  styleUrls: ['./data-details.component.scss']
})
export class DataDetailsComponent implements OnInit {

  currentD: any;
  //declare the variables
  chart_data: any;
  graph_data: any;
  label_data: any;

  //access the api data
  public subscription: Subscription;

  constructor(private echartService: EchartService) {

  }

  //initialize chart configurations parameters
  public lineChartData: ChartDataSets[] = [
    {
      data: [],
      lineTension: 0,
      backgroundColor: "transparent",
      borderColor: "rgb(107, 101, 101)"
    },
  ];

  public lineChartLabels: Label[];

  public lineChartOptions: (ChartOptions) = {
    responsive: true,
    maintainAspectRatio: false,

  };

  public lineChartLegend = false;
  public lineChartType = 'line' as ChartType;
  public lineChartPlugins = [];

  ngOnInit(): void {
    //access ing returned data of api service
    this.subscription = this.echartService.getBasicLineEchartData().subscribe(data => {
      this.graph_data = data;
      //fetch month data for x axis
      this.label_data = this.graph_data.map((t: any) => t.month)
      this.lineChartLabels = this.label_data;
      //fetch number data for y axis
      this.chart_data = this.graph_data.map((t: any) => t.message_count)
      this.lineChartData[0].data = this.chart_data;
    })

  }

}
