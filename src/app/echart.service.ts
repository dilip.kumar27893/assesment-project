import { Injectable } from "@angular/core";
import { HttpClient } from "@angular/common/http";

@Injectable()
export class EchartService {
    //route of api service
    apiURL = 'https://assessment-project-api.herokuapp.com/api/v0/get-graph-data';
    constructor(private httpClient: HttpClient) { }

    //method to call api service
    getBasicLineEchartData() {
        //get returned data
        return this.httpClient.get(this.apiURL);
    }

}