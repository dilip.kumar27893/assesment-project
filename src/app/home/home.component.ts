import { Component, OnInit } from '@angular/core';
import { ChartOptions, ChartType, ChartDataSets } from 'chart.js';
import { Color, Label } from 'ng2-charts';
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  //decalred variables
  public first_graph: ChartDataSets[];
  public second_graph: ChartDataSets[];
  public third_graph: ChartDataSets[];
  public forth_graph: ChartDataSets[];
  public fifth_graph: ChartDataSets[];
  public sixth_graph: ChartDataSets[];
  public seventh_graph: ChartDataSets[];
  public eighth_graph: ChartDataSets[];
  public ninth_graph: ChartDataSets[];
  public tenth_graph: ChartDataSets[];
  public eleventh_graph: ChartDataSets[];

  //chart options and it configuration
  public lineChartLabels: Label[] = ['January', 'February', 'March', 'April', 'May', 'June', 'July'];
  public lineChartOptions: (ChartOptions) = {
    responsive: true,
    maintainAspectRatio: false,
    scales: {
      xAxes: [
        { display: false }
      ],
      yAxes: [{
        display: false
      }]
    },
  };

  public lineChartLegend = false;
  public lineChartType = 'line' as ChartType;
  public lineChartPlugins = [];

  constructor() { }

  ngOnInit(): void {
    //assign value for charts
    this.first_graph = [
      {
        data: [65, 59, 80, 81, 56, 55, 40],
        lineTension: 0,
        backgroundColor: "rgb(197, 229, 235)",
        borderColor: "rgb(63, 208, 237)"
      },
    ];
    this.second_graph = [
      {
        data: [65, 53, 85, 77, 90, 81, 63],
        lineTension: 0,
        backgroundColor: "rgb(197, 229, 235)",
        borderColor: "rgb(63, 208, 237)"
      },
    ];
    this.third_graph = [
      {
        data: [85, 90, 82, 71, 78, 65, 56],
        lineTension: 0,
        backgroundColor: "rgb(197, 229, 235)",
        borderColor: "rgb(63, 208, 237)"
      },
    ];
    this.forth_graph = [
      {
        data: [52, 59, 40, 45, 86, 75, 60],
        lineTension: 0,
        backgroundColor: "rgb(197, 229, 235)",
        borderColor: "rgb(63, 208, 237)"
      },
    ];
    this.fifth_graph = [
      {
        data: [86, 77, 80, 73, 66, 55, 60],
        lineTension: 0,
        backgroundColor: "rgb(197, 229, 235)",
        borderColor: "rgb(63, 208, 237)"
      },
    ];
    this.sixth_graph = [
      {
        data: [65, 70, 41, 66, 75, 62, 44],
        lineTension: 0,
        backgroundColor: "rgb(197, 229, 235)",
        borderColor: "rgb(63, 208, 237)"
      },
    ];
    this.seventh_graph = [
      {
        data: [42, 56, 55, 37, 66, 78, 53],
        lineTension: 0,
        backgroundColor: "rgb(197, 229, 235)",
        borderColor: "rgb(63, 208, 237)"
      },
    ];
    this.eighth_graph = [
      {
        data: [62, 56, 67, 51, 80, 59, 43],
        lineTension: 0,
        backgroundColor: "rgb(197, 229, 235)",
        borderColor: "rgb(63, 208, 237)"
      },
    ];
    this.ninth_graph = [
      {
        data: [41, 51, 70, 61, 56, 65, 43],
        lineTension: 0,
        backgroundColor: "rgb(197, 229, 235)",
        borderColor: "rgb(63, 208, 237)"
      },
    ];
    this.tenth_graph = [
      {
        data: [30, 35, 36, 45, 40, 55, 40],
        lineTension: 0,
        backgroundColor: "rgb(197, 229, 235)",
        borderColor: "rgb(63, 208, 237)"
      },
    ];
    this.eleventh_graph = [
      {
        data: [61, 59, 70, 81, 56, 50, 45],
        lineTension: 0,
        backgroundColor: "rgb(197, 229, 235)",
        borderColor: "rgb(63, 208, 237)"
      },
    ];


  }

}
